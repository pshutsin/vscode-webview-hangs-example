import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(vscode.commands.registerCommand('catCoding.start', show))
}

const webViewSource = `
<html>
<body>
<div id="container"></div>
<script>
(function() {
	// const vscode = acquireVsCodeApi();
	const container = document.getElementById('container');

	const handleExtensionMessageEvent = function(event) {
	  console.log(event);
	  const block = document.createElement("div");
	  block.append(event.data.text);
	  container.append(block);
	}

	window.addEventListener('message', handleExtensionMessageEvent);
}())
</script>
</body>
</html>
`;

const show = async () => {
	const panel = vscode.window.createWebviewPanel(
		'catCoding',
		'Cat Coding',
		vscode.ViewColumn.One,
		{
			enableScripts: true,
		},
	);
	panel.webview.html = webViewSource;

	// This will work.
	// setTimeout(async () => {
	// 	await panel.webview.postMessage({text: 'hello'});
	// 	vscode.window.showInformationMessage('Execution continued after webview message processing.');
	// }, 100)

	// This will hang forever after "hello".
	await panel.webview.postMessage({text: 'hello'});
	vscode.window.showInformationMessage('Execution continued after webview message processing.');
}