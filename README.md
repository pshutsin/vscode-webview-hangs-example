This repository contains sample VSCode extension with "hanging webview" bug reproduced.

To reproduce the bug:

* run `npm watch`
* run extension in debug mode
* trigger "Start cat coding session" command.
* see `src/extension.ts` for details.
